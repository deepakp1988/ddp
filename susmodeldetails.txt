>> size(matA)

ans =

   148   148

>> size(matB)

ans =

   148    30

>> size(matC)

ans =

    24   148

>> size(matD)

ans =

    24    30
    
ABCD = [A B
        C D]


>> susmodel.in.gnd.disp

ans = 

  struct with fields:

    L: 1
    T: 2
    V: 3
    R: 6
    P: 5
    Y: 4

>> ingnd = susmodel.in.gnd.disp

ingnd = 

  struct with fields:

    L: 1
    T: 2
    V: 3
    R: 6
    P: 5
    Y: 4

>> intop = susmodel.in.top.disp
Reference to non-existent field 'disp'.
 
Did you mean:
>> intop = susmodel.in.top.drive

intop = 

  struct with fields:

    L: 7
    T: 8
    V: 9
    R: 12
    P: 11
    Y: 10

>> inuim = susmodel.in.uim.drive

inuim = 

  struct with fields:

    L: 13
    T: 14
    V: 15
    R: 18
    P: 17
    Y: 16

>> inuim = susmodel.in.uim.drive

inuim = 

  struct with fields:

    L: 13
    T: 14
    V: 15
    R: 18
    P: 17
    Y: 16

>> inpum = susmodel.in.pum.drive

inpum = 

  struct with fields:

    L: 19
    T: 20
    V: 21
    R: 24
    P: 23
    Y: 22

>> intst = susmodel.in.tst.drive

intst = 

  struct with fields:

    L: 25
    T: 26
    V: 27
    R: 30
    P: 29
    Y: 28

>> outgnd = susmodel.out.gnd.disp
Reference to non-existent field 'gnd'.
 
>> outpum = susmodel.out.pum.disp

outpum = 

  struct with fields:

    L: 13
    T: 14
    V: 15
    R: 18
    P: 17
    Y: 16

>> outtop = susmodel.out.top.disp

outtop = 

  struct with fields:

    L: 1
    T: 2
    V: 3
    R: 6
    P: 5
    Y: 4

>> outuim = susmodel.out.uim.disp

outuim = 

  struct with fields:

    L: 7
    T: 8
    V: 9
    R: 12
    P: 11
    Y: 10

>> outtst = susmodel.out.tst.disp

outtst = 

  struct with fields:

    L: 19
    T: 20
    V: 21
    R: 24
    P: 23
    Y: 22
