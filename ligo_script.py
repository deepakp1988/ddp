abcd = np.loadtxt('abcd.csv',delimiter = ',')
A = np.matrix(abcd[0:148,0:148])
B1 = np.matrix(abcd[0:148,148:])
C = np.matrix(abcd[148:,0:148])
D = np.matrix(abcd[148:,148:])

B = B1[:,6:] 
E = B1[:,:6] 
#H1 = C[[12,13,14,15,16,17],:] 
H2 = C[[18,19,20,21,22,23],:]

#V1 = maximal_contrl_invariant_space(A,B,H1)
#Fpart1 = set_of_friends(V1,A,B)[1]
#AF1 = A+B.dot(Fpart1)  

V2 = np.matrix(maximal_contrl_invariant_space(A,B,H2)) 
Fpart2 = set_of_friends(V2,A,B)[1]
AF2 = A+B.dot(Fpart2) 



#########################################################################################
#sys1 = ctrl.ss(AF1,E,C,D[:,:E.shape[1]]) 
sys2 = ctrl.ss(AF2,E,C,D[:,:E.shape[1]])

T = np.arange(0,10,0.001)
x0 = np.random.rand(148,1)*10
f=1
l = 2*pi*f
phi = np.zeros(E.shape[1]) 
for i in range(E.shape[1]):
    phi[i] = pi/(2*(i+1))
    
u1 = np.zeros((E.shape[1],len(T)))
for i in range(E.shape[1]):
    u1[i,:] = 10*np.sin(l*T+phi[i])
    
#np.random.rand(E.shape[1],len(T))*10     
#T,yout11, xout11 = ctrl.forced_response(sys1,T,u1,x0)
T,yout21, xout21 = ctrl.forced_response(sys2,T,u1,x0)

f=10
l = 2*pi*f
phi = np.zeros(E.shape[1]) 
for i in range(E.shape[1]):
    phi[i] = pi/(2*(i+1))
    
u2 = np.zeros((E.shape[1],len(T)))
for i in range(E.shape[1]):
    u2[i,:] = np.sin(l*T+phi[i])


#u2 = np.random.rand(E.shape[1],len(T))*10
#T,yout12, xout12 = ctrl.forced_response(sys1,T,u2,x0)
T,yout22, xout22 = ctrl.forced_response(sys2,T,u2,x0)

#er1 = yout11 - yout12
er2 = yout21 - yout22

#for i in [12,13,14,15,16,17]:
#    plot(T,er1[i,:])
figure(1)
text = ['L','T','V','Y','P','R']
for i in [18,19,20,21,22,23]:
    lines = plot(T,er2[i,:],label = text[i-18])
    
xlabel('time',fontsize=20)
ylabel('yout1 - yout2',fontsize=20)
title('comparison of output at test mass for two different inputs')
legend()
figure(2)
for i in range(18):
    lines1 = plot(T,er2[i,:])
     
xlabel('time',fontsize=20)
ylabel('yout1 - yout2',fontsize=20)
title('comparison of output at locations other than test mass for two different inputs',fontsize=20)

########################################################################################
#controllability subspace
##BsV, BuV = subspace_sum_intersect(B,V) #im B intersection with im V
##cs = kryl(AF,BuV)
##Q, R, e = spl.qr(cs,pivoting = True)
##Q = np.matrix(Q)
##Rs1 = np.empty((Q.shape[0],0),float)
##for i in range(min(R.shape)):
##    if abs(R[i,i])<100*machine_epsilon:
##       pass
##    else:
##       Rs1 = np.append(Rs, cs[:,e[i]], axis =1) 
 
S = np.zeros((A.shape[0],1))
ASB = subspace_sum_intersect(A.dot(S),B)[0]
S1 = subspace_sum_intersect(V,ASB)[1]
SS1= np.append(S,S1,axis=1)
while numpy.linalg.matrix_rank(S)<numpy.linalg.matrix_rank(SS1):
      S = S1
      ASB = subspace_sum_intersect(A.dot(S),B)[0]
      S1 = subspace_sum_intersect(V,ASB)[1]
      SS1= np.append(S,S1,axis=1)
 
S = S1    
SQ = np.linalg.qr(S)[0] 
#cab = kryl(A,B)
#Q1, R1, e = spl.qr(cab,pivoting = True)
#v = np.empty((Q1.shape[0],0),float)
#for i in range(min(R1.shape)):
#    if abs(R1[i,i])<10*-5:
#       pass
#    else:
#       v = np.append(v, cab[:,e[i]], axis =1)
########################################################################################
roots = -10*np.arange(0.01,1.49,0.01)
nr, mr = Rs1.shape 
p = roots[:mr]
q = roots[mr:]
######################################################################################
RsB = subspace_sum_intersect(S,B)[1]  # Rs \int B
Z = quotient(B,RsB) #(Rs \int B) \ds Z = B
RspZ = subspace_sum_intersect(S,Z)[0] #Rs \ds Z 
W1 = quotient(np.identity(148),RspZ) #(Rs \ds Z) \ds W1 = Rn
W = subspace_sum_intersect(Z,W1)[0] #W = Z \ds W1
WQ = np.linalg.qr(W)[0]
T2 = np.append(SQ,WQ,axis=1)  #T = Rs \ds W
######################################################################################
Anew = np.linalg.inv(T2).dot(A).dot(T2)
Bnew = np.linalg.inv(T2).dot(B)

nrsb,mrsb=RsB.shape
A11 = Anew[:mr,:mr]
A12 = Anew[:mr,mr:]
A22 = Anew[mr:,mr:]
A21 = Anew[mr:,:mr]
B1 = Bnew[:mr,:mrsb]
B2 = Bnew[mr:,mrsb:]

F21 = spl.lstsq(B2,-A21)[0]
F11 = ctrl.place(A11,B1,p)
F22 = ctrl.place(A22,B2,q)
F12 = np.zeros((F11.shape[0],F22.shape[1]))
F1 = np.append(-F11,F12,axis=1)
F2 = np.append(F21,-F22,axis=1)	
Fn = np.append(F1,F2,axis=0)
F = Fn.dot(np.linalg.inv(T2))
#########################################################################################

Af = A+B.dot(F)


sys3 = ctrl.ss(Af,E,C,D[:,:E.shape[1]])

T = np.arange(0,10,0.001)
x0 = np.random.rand(148,1)*10
f=1
l = 2*pi*f
phi = np.zeros(E.shape[1]) 
for i in range(E.shape[1]):
    phi[i] = pi/(2*(i+1))
    
u1 = np.zeros((E.shape[1],len(T)))
for i in range(E.shape[1]):
    u1[i,:] = np.sin(l*T+phi[i])
    
#np.random.rand(E.shape[1],len(T))*10     
#T,yout11, xout11 = ctrl.forced_response(sys1,T,u1,x0)
T,yout21, xout21 = ctrl.forced_response(sys3,T,u1,x0)

f=10
l = 2*pi*f
phi = np.zeros(E.shape[1]) 
for i in range(E.shape[1]):
    phi[i] = pi/(2*(i+1))
    
u2 = np.zeros((E.shape[1],len(T)))
for i in range(E.shape[1]):
    u2[i,:] = np.sin(l*T+phi[i])


#u2 = np.random.rand(E.shape[1],len(T))*10
#T,yout12, xout12 = ctrl.forced_response(sys1,T,u2,x0)
T,yout22, xout22 = ctrl.forced_response(sys3,T,u2,x0)

#er1 = yout11 - yout12
er2 = yout21 - yout22

#for i in [12,13,14,15,16,17]:
#    plot(T,er1[i,:])
figure(1)
for i in [18,19,20,21,22,23]:
    plot(T,er2[i,:])
    
xlabel('time')
ylabel('yout1 - yout2')
title('comparison of output at test mass for two different inputs')
 
figure(2)
for i in range(18):
    plot(T,er2[i,:])
     
xlabel('time')
ylabel('yout1 - yout2')
title('comparison of output at locations other than test mass for two different inputs')
