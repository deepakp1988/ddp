##Data taken from "Modal Damping of a Quadruple Pendulum for Advanced Gravitational Wave Detectors" by B. Shapiro, N Mavalvala, K Youcef-Toumi

A = np.matrix([[0,0,0,0,1,0,0,0],[0,0,0,0,0,1,0,0],[0,0,0,0,0,0,1,0],[0,0,0,0,0,0,0,1],[-297.3,163.5,0,0,0,0,0,0],[162.9,-267.2,104.2,0,0,0,0,0],[0,57.8,-74.2,16.4,0,0,0,0],[0,0,16.4,-16.4,0,0,0,0]])
B = np.matrix([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0.046,0,0,0],[0,0.045,0,0],[0,0,0.025,0],[0,0,0,0.25]])
E = np.matrix([[0],[0],[0],[0],[131.4],[0],[0],[0]])
H2 = np.matrix([0,0,0,1,0,0,0,0])
D = 0

C = np.matrix(np.identity(8)) 
  

V2 = np.matrix(maximal_contrl_invariant_space(A,B,H2)) 
Fpart2 = set_of_friends(V2,A,B)[1]
AF2 = A+B.dot(Fpart2) 



#########################################################################################
sys2 = ctrl.ss(AF2,E,C,D)

T = np.arange(0,10,0.001)
x0 = np.random.rand(8,1)*0.0000001
f=1
l = 2*pi*f
phi = np.zeros(E.shape[1]) 
for i in range(E.shape[1]):
    phi[i] = pi/(2*(i+1))
    
u1 = np.zeros((E.shape[1],len(T)))
for i in range(E.shape[1]):
    u1[i,:] = np.sin(l*T+phi[i])

T,xout21 = ctrl.forced_response(sys2,T,u1,x0)

f=10
l = 2*pi*f
phi = np.zeros(E.shape[1]) 
for i in range(E.shape[1]):
    phi[i] = pi/(2*(i+1))
    
u2 = np.zeros((E.shape[1],len(T)))
for i in range(E.shape[1]):
    u2[i,:] = 0.01*np.sin(l*T+phi[i])


T,xout22 = ctrl.forced_response(sys2,T,u2,x0)

er2 = xout21 - xout22


figure(1)
text = ['mass1','mass2','mass3','mass4']
for i in [3]:
    lines = plot(T,xout22[i,:],label = text[i])

xticks(fontsize=20)
yticks(fontsize=20)
grid()    
xlabel('time',fontsize=20)
ylabel('displacement of mass4',fontsize=20)
legend(fontsize=20)
title('disturbance of 10hz sinewave of amplitude 0.01',fontsize=20)

figure(2)
text = ['mass1','mass2','mass3','mass4']
for i in [0,1,2]:
    lines = plot(T,xout22[i,:],label = text[i])

xticks(fontsize=20)
yticks(fontsize=20)
grid()    
xlabel('time',fontsize=20)
ylabel('displacement of masses 1,2,3',fontsize=20)
legend(fontsize=20)
title('disturbance of 10 hz sinewave of amplitude 0.01',fontsize=20)

figure(3)
text = ['mass1','mass2','mass3','mass4']
for i in [0,1,2,3]:
    lines = plot(T,er2[i,:],label = text[i])

xticks(fontsize=20)
yticks(fontsize=20)
grid()    
xlabel('time',fontsize=20)
ylabel('difference of outputs for two different disturbances',fontsize=20)
legend(fontsize=20)
title('effect of 1 hz and 10 hz disturbance',fontsize=20)


########################################################################################
#controllability subspace

BsV, BuV = subspace_sum_intersect(B,V2) #im B intersection with im V
cs = kryl(AF2,BuV)
Q, R, e = spl.qr(cs,pivoting = True)
Q = np.matrix(Q)
Rs1 = np.empty((Q.shape[0],0),float)
for i in range(min(R.shape)):
    if abs(R[i,i])<100*machine_epsilon:
       pass
    else:
       Rs1 = np.append(Rs1, cs[:,e[i]], axis =1) 
       
#cab = kryl(A,B)
#Q1, R1, e = spl.qr(cab,pivoting = True)
#v = np.empty((Q1.shape[0],0),float)
#for i in range(min(R1.shape)):
#    if abs(R1[i,i])<10*-5:
#       pass
#    else:
#       v = np.append(v, cab[:,e[i]], axis =1)

########################################################################################
roots = -1*np.arange(1,5,0.5)
nr, mr = Rs1.shape 
p = roots[:mr]
q = roots[mr:]

######################################################################################
RsB = subspace_sum_intersect(Rs1,B)[1]  # Rs \int B
Z = quotient(B,RsB) #(Rs \int B) \ds Z = B
RspZ = subspace_sum_intersect(Rs1,Z)[0] #Rs \ds Z 
W1 = quotient(np.identity(8),RspZ) #(Rs \ds Z) \ds W1 = Rn
W = subspace_sum_intersect(Z,W1)[0] #W = Z \ds W1
T2 = np.append(Rs1,W,axis=1)  #T = Rs \ds W

######################################################################################
Anew = np.linalg.inv(T2).dot(A).dot(T2)
Bnew = np.linalg.inv(T2).dot(B)

nrsb,mrsb=RsB.shape
A11 = Anew[:mr,:mr]
A12 = Anew[:mr,mr:]
A22 = Anew[mr:,mr:]
A21 = Anew[mr:,:mr]
B1 = Bnew[:mr,:mrsb]
B2 = Bnew[mr:,mrsb:]

F21 = spl.lstsq(B2,-A21)[0]
F11 = ctrl.place(A11,B1,p)
F22 = ctrl.place(A22,B2,q)
F12 = np.zeros((F11.shape[0],F22.shape[1]))
F1 = np.append(-F11,F12,axis=1)
F2 = np.append(F21,-F22,axis=1)	
Fn = np.append(F1,F2,axis=0)
F = Fn.dot(np.linalg.inv(T2))
#########################################################################################

Af = A+B.dot(F)
sys3 = ctrl.ss(Af,E,C,D)



T = np.arange(0,10,0.001)
x0 = np.random.rand(8,1)*0.0000001
f=1
l = 2*pi*f
phi = np.zeros(E.shape[1]) 
for i in range(E.shape[1]):
    phi[i] = pi/(2*(i+1))
    
u1 = np.zeros((E.shape[1],len(T)))
for i in range(E.shape[1]):
    u1[i,:] = np.sin(l*T+phi[i])
    

T, xout31 = ctrl.forced_response(sys3,T,u1,x0)

f=10
l = 2*pi*f
phi = np.zeros(E.shape[1]) 
for i in range(E.shape[1]):
    phi[i] = pi/(2*(i+1))
    
u2 = np.zeros((E.shape[1],len(T)))
for i in range(E.shape[1]):
    u2[i,:] = 0.01*np.sin(l*T+phi[i])


T, xout32 = ctrl.forced_response(sys3,T,u2,x0)


er2 = xout31 - xout32


figure(4)
text = ['mass1','mass2','mass3','mass4']
for i in [3]:
    lines = plot(T,xout32[i,:],label = text[i])

xticks(fontsize=20)
yticks(fontsize=20)
grid()    
xlabel('time',fontsize=20)
ylabel('displacement of mass4',fontsize=20)
legend(fontsize=20)
title('disturbance of 10 hz sinewave of amplitude 0.01',fontsize=20)

figure(5)
text = ['mass1','mass2','mass3','mass4']
for i in [0,1,2]:
    lines = plot(T,xout32[i,:],label = text[i])

xticks(fontsize=20)
yticks(fontsize=20)
grid()    
xlabel('time',fontsize=20)
ylabel('displacement of masses 1,2,3',fontsize=20)
legend(fontsize=20)
title('disturbance of 10 hz sinewave of amplitude 0.01',fontsize=20)
sys4 = ctrl.ss(Af,E,H2,D)
